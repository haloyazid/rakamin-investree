package com.investree.demo.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "Transaksi")
public class Transaksi implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_peminjam")
    User peminjam;

    @ManyToOne
    @JoinColumn(name = "id_meminjam")
    User meminjam;

    @Column(name = "tenor", nullable = false)
    private int tenor;

    @Column(name = "total_pinjaman", nullable = false)
    private double totalPinjaman;

    @Column(name = "bunga_persen")
    private double bungaPersen;

    @Column(name = "status", nullable = false)
    private String status;

    @OneToMany(mappedBy = "transaksi")
    private List<PaymentHistory> history;

    public Long getId() {
        return id;
    }

    public List<PaymentHistory> getHistory() {
        return history;
    }

    public void setHistory(List<PaymentHistory> history) {
        this.history = history;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getPeminjam() {
        return peminjam;
    }

    public void setPeminjam(User peminjam) {
        this.peminjam = peminjam;
    }

    public User getMeminjam() {
        return meminjam;
    }

    public void setMeminjam(User meminjam) {
        this.meminjam = meminjam;
    }

    public int getTenor() {
        return tenor;
    }

    public void setTenor(int tenor) {
        this.tenor = tenor;
    }

    public double getTotalPinjaman() {
        return totalPinjaman;
    }

    public void setTotalPinjaman(double totalPinjaman) {
        this.totalPinjaman = totalPinjaman;
    }

    public double getBungaPersen() {
        return bungaPersen;
    }

    public void setBungaPersen(double bungaPersen) {
        this.bungaPersen = bungaPersen;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
