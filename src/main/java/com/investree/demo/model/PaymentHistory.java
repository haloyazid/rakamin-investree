package com.investree.demo.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "payment_history")
public class PaymentHistory implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_transaksi")
    Transaksi transaksi;

    @Column(name = "pembayaran_ke", nullable = false)
    private int pembayaranKe = 0;

    @Column(name = "jumlah", nullable = false)
    private double jumlah;

    @Column(name = "bukti_pembayaran", nullable = false)
    private String buktiPembayaran;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Transaksi getTransaksi() {
        return transaksi;
    }

    public void setTransaksi(Transaksi transaksi) {
        this.transaksi = transaksi;
    }

    public int getPembayaranKe() {
        return pembayaranKe;
    }

    public void setPembayaranKe(int pembayaranKe) {
        this.pembayaranKe = pembayaranKe;
    }

    public double getJumlah() {
        return jumlah;
    }

    public void setJumlah(double jumlah) {
        this.jumlah = jumlah;
    }

    public String getBuktiPembayaran() {
        return buktiPembayaran;
    }

    public void setBuktiPembayaran(String buktiPembayaran) {
        this.buktiPembayaran = buktiPembayaran;
    }
}
