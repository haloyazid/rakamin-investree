package com.investree.demo.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username", nullable = false, length = 50, unique = true)
    private String username;

    @Column(name = "password", nullable = false, length = 50, unique = true)
    private String password;

    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private boolean isActive;

    @OneToOne(mappedBy = "detailUser")
    private UserDetail detail;

    @OneToMany(mappedBy = "peminjam")
    private List<Transaksi> peminjam;

    @OneToMany(mappedBy = "meminjam")
    private List<Transaksi> meminjam;

    public Long getId() {
        return id;
    }

    public List<Transaksi> getPeminjam() {
        return peminjam;
    }

    public void setPeminjam(List<Transaksi> peminjam) {
        this.peminjam = peminjam;
    }

    public List<Transaksi> getMeminjam() {
        return meminjam;
    }

    public void setMeminjam(List<Transaksi> meminjam) {
        this.meminjam = meminjam;
    }

    public UserDetail getDetail() {
        return detail;
    }

    public void setDetail(UserDetail detail) {
        this.detail = detail;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
